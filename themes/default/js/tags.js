function tagInitialize() {
    return {
        tags: [],
        selected: null,
        toots: [],
        tootsFetched: false,
        getTagsJson() {
            let myThis = this;
            fetch('js/tags.json')
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
            .then((res) => {
                myThis.tags = res;
                if (window.location.hash) {
                    let selected = decodeURIComponent(window.location.hash.substr(1));
                    if (typeof(myThis.tags[selected]) !== 'undefined') {
                        myThis.selected = selected;
                    } else {
                        window.location.hash = '';
                    }
                }
                fetch('js/search.json')
                .then((response) => {
                    if (!response.ok) {
                        throw new Error(`HTTP error! status: ${response.status}`);
                    }
                    return response.json();
                })
                .then((res) => {
                    myThis.toots        = res;
                    myThis.tootsFetched = true;
                });
            });
        },
        changeSelectedTag() {
            this.selected = this.$refs.tagName.value;
            window.location.hash = '#'+this.selected;
        }
    }
}
